<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GenreController;

// use App\Http\Controllers\BiodataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/daftar', [BiodataController::class, 'bio']);

Route::post('/home', [BiodataController::class, 'SingUp']);

Route::get('/data-table' , function(){
    return view('halaman.data-table');
});


Route::get('/genre/create', [GenreController::class, 'create']);

Route::post('/genre', [GenreController::class, 'store']);

Route::get('/genre', [GenreController::class, 'index']);

Route::get('/genre/{genre_id}', [GenreController::class, 'show']);

Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);

Route::put('/genre/{genre_id}', [GenreController::class, 'update']);



