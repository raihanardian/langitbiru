@extends('layout.master')
@section('title')
    Halaman Tambah Genre
@endsection

@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input text="text" nama="nama" class="form-control">
    </div>
    @error('title')
    <div class="alret akret-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Deskripsi Genre</label>
        <textarea name="deskripsi" class="from-control" cols="30" rows="10"></textarea>
    </div>
    @error('title')
    <div class="alret akret-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection