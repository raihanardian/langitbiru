@extends('layout.master')
@section('title')
    Halaman Edit Genre
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Genre</label>
      <input text="text" value="{{$genre->nama}}" nama="nama" class="form-control">
    </div>
    @error('title')
    <div class="alret akret-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Deskripsi Genre</label>
        <textarea name="deskripsi" class="from-control" cols="30" rows="10">{{$gente->deskripsi}}</textarea>
    </div>
    @error('title')
    <div class="alret akret-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection