@extends('layout.master')
@section('title')
    Detail Genre
@endsection

@section('content')
<h1>{{$genre->nama}}</h1>
<p>{{$genre->deskripsi}}</p>

<a href="/kategori" class="btn btn-secondary btn-sm">kembali</a>
@endsection