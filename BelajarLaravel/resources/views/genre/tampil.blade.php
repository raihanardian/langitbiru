@extends('layout.master')
@section('title')
    Halaman List Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm mb-3">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>
                <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/genre/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            </td>
        </tr>
    @empty
        <p>No users</p>
    @endforelse
    </tbody>
  </table>

@endsection