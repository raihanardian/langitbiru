<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Halaman Biodata</h1>
    <form action="/home" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="nama_depan"><br><br>

        <label>Last Name:</label><br>
        <input type="text" name="nama_belakang"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="jenis_kelamin" value="male">Male<br>
        <input type="radio" name="jenis_kelamin" value="female">Female<br>
        <input type="radio" name="jenis_kelamin" value="other">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="Nationality">
            <option value="indonesia">Indonesia</option>
            <option value="english">English</option>
            <option value="other">Other</option>
        </select><br><br>

        <label>Laguage Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br>

        <label>Bio</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea><br>
        <input type="Submit" value="Sing Up">
     </form>
    
</body>
</html>