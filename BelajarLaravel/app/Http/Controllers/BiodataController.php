<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function bio()
    {
        return view('halaman.biodata');
    }

    public function SingUp(Request $request)
    {
        $fullname = $request['nama_depan'];
        $lastname = $request ['nama_belakang'];


        return view('halaman.home', ['fullname' => $fullname], ['lastname' => $lastname]);
    }
}
