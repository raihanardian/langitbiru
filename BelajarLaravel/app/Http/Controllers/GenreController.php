<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create(){
        return view('genre.tambah');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required'
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi']
        ]);

        return redirect('/genre');
    }

    public function index()
    {

        $genre = DB::table('genre')->get();

        return view('genre.tampil', ['genre' => $genre]);
    }

    public function show($id)
    {
        $genre = DB::table('genre')->where('id', '$id')->first();

        return view('genre.detail', ['genre' => $genre]);
    }

    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', '$id')->first();

        return view('genre.edit', ['genre' => $genre]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required'
        ]);
        
        DB::table('genre')
        ->insert('id', $id)
        ->update(
            ['nama' => $request->nama],
            ['deskripsi' => $request->deskripsi]
        );

        return redirect('/genre');
    }

}
