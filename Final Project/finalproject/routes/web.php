<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\profileController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\pertanyaan2Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\profile2Controller;
use App\Http\Controllers\komentarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware(['auth', 'CheckRole:admin'])->group(function(){
    Route::get('/profile', [profileController::class, 'index']);
    Route::delete('/profile/{profile_id}/hapus', [profileController::class, 'destroy']);
    Route::get('/kategori', [kategoriController::class, 'index']);
    Route::delete('/kategori/{kategori_id}/hapus', [kategoriController::class, 'destroy']);
    Route::get('/pertanyaan', [pertanyaan2Controller::class, 'index']);
    Route::get('/komentar', [komentarController::class, 'index']);
    Route::delete('/komentar/{komentar_id}/hapus', [komentarController::class, 'destroy']);
});

Route::group(['middleware' => ['auth', 'CheckRole:admin,user']],function(){
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/profile/{profile_id}/profile', [profileController::class, 'profile']);
    Route::get('/forum',[PertanyaanController::class, 'index']);
    Route::get('/profile/{profile_id}/edit', [profileController::class, 'edit']);
    Route::put('/profile/{profile_id}', [profileController::class, 'update']);
    Route::resource('editprofile', profile2Controller::class)->only(['index','update']);
    Route::get('forum/add', [PertanyaanController::class, 'add'])->name('forum.add');
    Route::post('forum/create', [PertanyaanController::class, 'create']);
    Route::get('/forum/{forum_id}/view', [PertanyaanController::class, 'view']);
    Route::post('/forum/{forum_id}/view', [PertanyaanController::class, 'postkomentar']);
    Route::delete('/forum/{forum_id}/hapus', [PertanyaanController::class, 'destroy']);
});

Auth::routes();
