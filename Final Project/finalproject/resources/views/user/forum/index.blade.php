@extends('master')

@section('content')

<div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="panel">
            <div class="panel-heading">
                <h1 class="panel-title"><b>Forum</b></h1>
            </div>
              <div class="panel-body">
                <ul class="list-unstyled activity-list">
                  @foreach($pertanyaan as $tanya)
                    <li>
                      <div class="text-muted small text-center align-self-center">
                        Tags :
                        @foreach ($tanya->kategori as $tag)
                        <button class="btn btn-primary btn-sm">{{$tag->tag_name ? $tag->tag_name:'No tags' }}</button>
                        @endforeach
                      </div>
                      <img src="{{$tanya->user->profile->getAvatar()}}" alt="Avatar" class="img-circle pull-left avatar">
                      <p><a href="/forum/{{ $tanya->id }}/view">{{ $tanya->user->profile->name }} : {{ $tanya->judul }} <span class="timestamp">{{ $tanya->created_at->diffForHumans() }}</span></a></p>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection