@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Ajukan Pertanyaan</b></h1>
            </div>
            <div class="panel-body">
                <div class="row">
                    <form action="/forum/create" method="post" enctype="multipart/form-data">
                      @csrf
                    <div class="col-lg-8">
                            <div class="form-group">
                              <label>Judul</label>
                              <input type="string" name="judul" class="form-control" placeholder="Masukan Judul">
                            </div>
                            @error('judul')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label>Tanyakan</label>
                                <textarea name="isi" class="form-control" id="isi" rows="10"></textarea>
                            </div>
                            @error('isi')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label for="kategori">Hashtag</label>
                            <input type="text" class="form-control" id="kategori" name="kategori" placeholder="contoh : satu,sdua,tiga">
                          </div>

                          <div class="input-group">
                            <span class="input-group-btn">
                              <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Choose
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="filepath">
                          </div>
                          <img id="holder" style="margin-top:15px;max-height:100px;">
                            
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning">Kirim</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footer')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
  <script>
    ClassicEditor
      .create(document.querySelector('#isi'))
      .catch( error => {
        console.error(error);
      });
      $(document).ready(function(){
        $('#lfm').filemanager('image');
      });
  </script>
@endsection