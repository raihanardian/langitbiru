@extends('master')

@section('content')

<div class="main">
    <div class="main-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
                <!-- PANEL HEADLINE -->
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <a href="#">{{ $pertanyaan->user->profile->name }}</a> 
                        <img src="{{ $pertanyaan->user->profile->getAvatar() }}" width="50" alt="Avatar" class="img-circle pull-left avatar">
                        <h3 class="panel-title">{{ $pertanyaan->judul }}</h3>
                        <p class="panel-subtitle">{{ $pertanyaan->created_at->diffForHumans() }}</p>
                    </div>
                    <div class="panel-body">
                        {{ $pertanyaan->isi }}
                        <hr>
                    <h3>Komentar</h3>
                    <form action="" method="POST">
                        @csrf
                        <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                        <input type="hidden" name="parent" value="0">
                        <textarea style="margin-bottom: 10px" name="isi" class="form-control" id="isi" rows="4"></textarea>
                        <input type="submit" class="btn btn-primary" value="kirim">
                    </form>
                        <ul class="list-unstyled activity-list">
                            @foreach($pertanyaan->komentar()->where('parent',0)->orderBy('created_at','desc')->get() as $komentar)
                            <li>
                                <img src="{{ $komentar->user->profile->getAvatar() }}" alt="Avatar" class="img-circle pull-left avatar">
                                <p><a href="#">{{ $komentar->user->profile->name }}</a> 
                                    <br> 
                                {{ $komentar->isi }}    
                                <span class="timestamp">{{ $komentar->created_at->diffForHumans() }}</span></p>
                                <form action="" method="POST" style="padding-left: 3.5em; padding-right: 3.5em;">
                                    @csrf
                                    <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                                    <input type="hidden" name="parent" value="{{ $komentar->id }}">
                                    <textarea style="margin-bottom: 5px" name="isi" class="form-control" id="isi" rows="2"></textarea>
                                    <input type="submit" class="btn btn-primary btn-xs" value="Kirim">
                                </form>
                                <br>
                                <form action="" style="padding-left: 3.5em; padding-right: 3.5em;">
                                    @foreach ($komentar->childs()->orderBy('created_at','desc')->get() as $child)
                                        <img src="{{ $child->user->profile->getAvatar() }}" alt="Avatar" class="img-circle pull-left avatar">
                                        <p><a href="#">{{ $child->user->profile->name }} : </a>
                                        {{ $child->isi }}    
                                        <span class="timestamp">{{ $child->created_at->diffForHumans() }}</span></p>
                                        <br>
                                    @endforeach
                                </form>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- END PANEL HEADLINE -->
          </div>
        </div>
      </div>
    </div>
</div>

@endsection