<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="index.html"><img src="{{asset('/images/logo.png') }}" alt="Klorofil Logo" class="img-responsive logo" width="75"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <form class="navbar-form navbar-left">
            <div class="input-group">
                <input type="text" name="cari" value="" class="form-control" placeholder="Search dashboard...">
                <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
            </div>
        </form>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{auth()->user()->profile->getAvatar()}}" class="img-circle">
                        
                        <span>{{ auth()->user()->name }}</span> 
                       
                        <i class="icon-submenu lnr lnr-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        @auth
                            <li class="nav-item">
                                <a href="/editprofile" class="nav-link">
                                    <i class="lnr lnr-user"></i>
                                    <span>My Profile</span>
                                </a>
                            </li>
                        @endauth
                        <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
                        <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
                    @guest
                        <li class="nav-item bg-info">
                        <a href="/login" class="nav-link">
                            <i class="lnr lnr-cog"></i>
                            <p>
                                Login
                            </p>
                        </a>
                        </li>
                    @endguest
                    @auth
                        <li class="nav-item bg-danger">
                            <a class="nav-link" style="color: #ffffff" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                        </ul>
                    </li>
                    @endauth
            </ul>
        </div>
    </div>
</nav>