<div id="sidebar-nav" class="sidebar">
  <div class="sidebar-scroll">
    <nav>
      <ul class="nav">
        <li><a href="{{ route('forum.add') }}" class="btn btn-primary" style="color: #ffffff"><i class="fas fa-plus"></i></i><span>Tambah Pertanyaan</span></a></li>
        @if(auth()->user()->role == 'admin')
        <li>
          <a href="#subPages" data-toggle="collapse" class="active" aria-expanded="true"><i class="fas fa-server"></i><span>Data Table</span><i class="icon-submenu lnr lnr-chevron-down"></i></a>
          <div id="subPages" class="collapse in" aria-expanded="true" style="">
            <ul class="nav">
              <li><a href="/profile" class="active"><i class="fas fa-database"></i><span>Data Profile User</span></a></li>
              <li><a href="/kategori" class="active"><i class="fas fa-database"></i><span>Data Kategori</span></a></li>
              <li><a href="/pertanyaan" class="active"><i class="fas fa-database"></i><span>Data Pertanyaan</span></a></li>
              <li><a href="/komentar" class="active"><i class="fas fa-database"></i><span>Data Komentar</span></a></li>
            </ul>
          </div>
        </li>
        @endif
        <li><a href="/forum" class="active"><i class="fas fa-clipboard-list"></i><span>Forum</span></a></li>
      </ul>
    </nav>
  </div>
</div>