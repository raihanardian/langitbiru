@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Table Pertanyaan</b></h1>
              
            </div>
            <div class="panel-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Pertanyaan</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($pertanyaan as $forum)
                  <tr>
                    <td><a href="/tanya/{{ $forum->id }}/profile">{{ $forum->user->profile->name }}</a></td>
                    <td><a href="/tanya/{{ $forum->id }}/profile">{{ $forum->user->profile->email }}</a></td>
                    <td>{{ $forum->judul }}</td>
                    <td>{{ $forum->isi }}</td>
                    <td>
                      <a href="/forum/{{ $forum->id }}/view" class="btn  btn-success" style="margin-bottom: 5px">SHOW</a>
                      <a href="/tanya/{{ $forum->id }}/edit" class="btn  btn-primary" style="margin-bottom: 5px">EDIT</a>
                      <form action="/forum/{{$forum->id}}/hapus" method="POST" class="d-inline">
                          @method('delete')
                          @csrf
                      <button  class="submit btn badge-danger">hapus </button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection