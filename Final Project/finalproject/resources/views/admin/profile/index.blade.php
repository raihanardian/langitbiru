@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Table Profile</b></h1>
              
            </div>
            <div class="panel-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Biodata</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data_profile as $profile)
                  <tr>
                    <td><a href="/profile/{{ $profile->id }}/profile">{{ $profile->name }}</a></td>
                    <td><a href="/profile/{{ $profile->id }}/profile">{{ $profile->email }}</a></td>
                    <td>{{ $profile->umur }}</td>
                    <td>{{ $profile->alamat }}</td>
                    <td>{{ $profile->biodata }}</td>
                    <td>
                      <a href="/profile/{{ $profile->id }}/profile" class="btn  btn-success" style="margin-bottom: 5px">SHOW</a>
                      <a href="/profile/{{ $profile->id }}/edit" class="btn  btn-primary" style="margin-bottom: 5px">EDIT</a>
                      <form action="profile/{{$profile->id}}/hapus" method="POST" class="d-inline">
                          @method('delete')
                          @csrf
                      <button  class="submit btn badge-danger">hapus </button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection