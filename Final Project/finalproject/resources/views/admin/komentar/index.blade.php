@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Table Komentar</b></h1>
              
            </div>
            <div class="panel-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Komentar</th>
                    <th scope="col">Balasan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($komentar as $komen)
                  <tr>
                    <td>{{ $komen->id }}</td>
                    <td><a href="/komentar/{{ $komen->id }}/profile">{{ $komen->user->profile->name }}</a></td>
                    <td><a href="/komentar/{{ $komen->id }}/profile">{{ $komen->user->profile->email }}</a></td>
                    <td>{{ $komen->isi }}</td>
                    <td>{{ $komen->parent }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection