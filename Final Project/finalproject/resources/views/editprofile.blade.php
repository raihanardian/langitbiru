@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Edit Data Profile</b></h1>
              
            </div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <form action="/profile/{{ $profile->id }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <div class="form-group">
                          <label>Avatar</label>
                          <input type="file" name="avatar" class="form-control" value="{{ $profile->avatar }}">
                        </div>
                        @error('avatar')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                          <label>Nama</label>
                          <input type="string" name="name" class="form-control" value="{{ $profile->name }}">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $profile->email }}">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                          <label>Umur</label>
                          <input type="integer" name="umur" class="form-control" value="{{ $profile->umur }}">
                        </div>
                        @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                          <label>Alamat</label>
                          <input type="string" name="alamat" class="form-control" value="{{ $profile->alamat }}">
                        </div>
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                          <label>Biodata</label>
                          <textarea name="biodata" class="form-control" rows="5">{{ $profile->biodata }}</textarea>
                        </div>
                        @error('biodata')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection