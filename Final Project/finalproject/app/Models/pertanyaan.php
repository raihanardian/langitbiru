<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['id','judul','content','tag','kategori_id','users_id'];
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'users_id','id');
    }

    public function kategori(){
        return $this->belongsToMany(kategori::class,'pertanyaan_tag','pertanyaan_id','kategori_id');
    }

    public function komentar(){
        return $this->hasMany(komentar::class);
    }
}
