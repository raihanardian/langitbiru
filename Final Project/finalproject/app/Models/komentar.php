<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ['isi', 'parent', 'users_id', 'pertanyaan_id'];

    public function pertanyaan(){
        return $this->belongsTo(pertanyaan::class,'pertanyaan_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'users_id','id');
    }
    public function childs(){
        return $this->hasMany(komentar::class,'parent');
    }
    
}
