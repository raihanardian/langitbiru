<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['name', 'umur', 'email', 'alamat', 'biodata', 'avatar', 'users_id'];
    
    public function getAvatar()
    {
        if(!$this->avatar)
        {
            return asset('images/default.png');
        }
        return asset('images/'.$this->avatar);
    }
    
    public function user(){
        return $this->hasOne(User::class ,'users_id','id');
    }

    public function pertanyaan(){
        return $this->hasMany(pertanyaan::class);
    }

    public function komentar(){
        return $this->hasMany(komentar::class,'profile_id','id');
    }

}
