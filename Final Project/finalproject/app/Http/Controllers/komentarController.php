<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\profile;
use App\Models\pertanyaan;
use App\Models\komentar;
use App\Models\User;

class komentarController extends Controller
{
    public function index(Request $request)
    {
        $komentar = komentar::orderBy('created_at','desc')->get();
        return view('admin.komentar.index',compact(['komentar']));
    }

    public function destroy(pertanyaan $pertanyaan, $id)
    {
        komentar::where('id', $id)->delete();
        komentar::where('parent', $id)->delete();
        return redirect('/forum')->with('eror', 'data anda berhasil di hapus');
    }
}
