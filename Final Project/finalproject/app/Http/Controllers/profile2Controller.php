<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\profile;
use App\Models\User;

class profile2Controller extends Controller
{
    public function index()
    {
        $iduser = Auth::id();
        $detailprofile = profile::where('users_id', $iduser)->first();
        return view('user.profilsaya',['detailprofile' => $detailprofile]);

    }
}
