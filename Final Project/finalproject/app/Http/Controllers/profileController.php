<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\profile;
use App\Models\User;
use App\Models\pertanyaan;
use App\Models\komentar;



class profileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index(Request $request)
    {
        if($request->has('cari'))
        {
            $data_profile = profile::Where('email', 'LIKE', '%'.$request->cari.'%')
                            ->orWhere('name', 'LIKE', '%'.$request->cari.'%')
                            ->orWhere('alamat', 'LIKE', '%'.$request->cari.'%')
                            ->get();
        }else{
            $data_profile = profile::orderBy('created_at','desc')->get();
        };
        return view('admin.profile.index', ['data_profile' => $data_profile]);
    }

    public function profile($id)
    {
        $profile = profile::find($id);
        return view('admin.profile.profile',['profile' => $profile]);
    }

    public function edit($id)
    {
        $profile = profile::find($id);
        return view('editprofile',['profile' => $profile]);
    }
    
    public function update(Request $request, $id)
    {
        $profile = profile::find($id);
        $profile->update($request->all());
        if($request->hasFile('avatar'))
        {
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $profile->avatar=$request->file('avatar')->getClientOriginalName();
            $profile->save();
        }
        return redirect('/forum')->with('sukses','Data berhasil update');
    }

    public function destroy(profile $profile, $id)
    {
        profile::where('id', $id)->delete();
        User::where('id', $id)->delete();
        pertanyaan::where('id', $id)->delete();
        komentar::where('id', $id)->delete();
        komentar::where('parent', $id)->delete();
        return redirect('/profile')->with('eror', 'data anda berhasil di hapus');
    }
}
