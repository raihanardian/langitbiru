<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\pertanyaan;
use App\Models\kategori;
use App\Models\User;
use App\Models\komentar;

class pertanyaan2Controller extends Controller
{
    public function index(Request $request)
    {
        $pertanyaan = pertanyaan::orderBy('created_at','desc')->get();
        return view('admin.forum.index',compact(['pertanyaan']));
    }
}
