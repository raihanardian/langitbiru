<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pertanyaan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if($request->has('cari'))
        {
            $pertanyaan = pertanyaan::Where('judul', 'LIKE', '%'.$request->cari.'%')
                            ->get();
        }else{
            $pertanyaan = pertanyaan::orderBy('created_at','desc')->get();
        };
        return view('user.forum.index',compact(['pertanyaan']));
    }
}
