<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\profile;
use App\Models\User;
use App\Models\pertanyaan;
use App\Models\kategori;

class kategoriController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari'))
        {
            $data_kategori = kategori::Where('tag_name', 'LIKE', '%'.$request->cari.'%')->get();
        }else{
            $data_kategori = kategori::orderBy('created_at','desc')->get();
        };
        return view('admin.kategori.index', ['data_kategori' => $data_kategori]);
    }

    public function destroy(profile $profile, $id)
    {
        kategori::where('id', $id)->delete();
        return redirect('/kategori')->with('eror', 'data anda berhasil di hapus');
    }
}
